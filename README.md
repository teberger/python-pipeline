# Python-Pipeline

# GitLab Python Pipeline 

---

* Step 1: Register/Login to [gitlab](https://gitlab.com/users/sign_in)

* Step 2: [Create a gitlab project](https://gitlab.com/projects/new)

  * Click to `import project` icon

  * Click to `Repo by URL`

  * Fill `https://gitlab.com/ti1akt/python-pipeline.git` as the `Git repository URL`, specify a project name and click `create the project` button

```bash
https://gitlab.com/ti1akt/python-pipeline.git
```

* Step 3: Once the repository has been imported, all files should be visible in the project, and the pipeline will be automatically triggered

* Step 4: `.gitlab-ci.yml` is the file that is configured to build the docker image and run specified scans.

  * `bandit_sast` runs a [bandit](https://github.com/PyCQA/bandit) SAST scan and `Fails` if there are findings. 

    * `allow_failure: true` option ignores the failure and continues executing the pipeline.

  * `pyraider_dependency_scanning` runs a [pyraider](https://pyraider.raidersource.com/) SCA scan on the dependencies

  * `build` containerizes the application and stores the container on the GitLab registry. 

    * The docker image can be found in `Container Registry` under `Packages & Registries` that's available in the sidebar

  * [`shcheck`](https://github.com/meliot/shcheck) runs the application container that was built in the previous step and checks the security headers

* Step 5: Select `CI/CD` on the sidebar to view the pipeline.

* Step 6: Wait for the pipeline to execute and observe the results
